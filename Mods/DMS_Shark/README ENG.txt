===============================================================================
Mod author: DemiromiS
This modification adds a new vehicle to the game. 
Installing in the root folder of the game 7 Days To Die
===============================================================================
If you want to support the author of the modification.
Wallet for donations:
WebMoney USD: Z100779190942 
WebMoney EUR: E127404900415
WebMoney RUB: R193833136417
Yandex RUB: 4100110790648864
===============================================================================
3D model of transport was taken on freely distributed resources of the Internet. 
Model Shark: https://assetstore.unity.com/packages/3d/vehicles/land/race-game-car-bmw-shark-137732 
=============================================================================== 
Allows
- You have the right to use the mod for entertainment purposes.
- You have the right to use the mod on your server.
Not allowed
- You do not have the right to make changes without the consent of the author. 
- You have no right to sell the modification. 
===============================================================================
