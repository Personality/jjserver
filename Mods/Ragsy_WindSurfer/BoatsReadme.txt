Experimental work in progress version 1.


Launch

Place the Windsurfer on/in water in a 'shallow water' area that has fairly flat ground underneath

Controls

Steering is A and D as per vanilla settings ....mouse is for looking around only and has no affect on steering a water vehicle.
Space bar and c control the up and down angles (same as a gyro) which you may occasionally need to adjust slightly as you travel on water otherwise you may sink, this version seems very stable as its a slower speed water vehicle.  
W is forwards and S is slow down/reverse ..If you let go of w and shift (Turbo) then the Windsurfer slowly comes to a stop, pressing S will slow the Windsurfer down
more quickly this replaces spacebar as the brake key.

To anchor the Windsurfer and stop it drifting away use C and spacebar to level the Windsurfer till it stops drifting.

Particle system added ...this can be toggled via the 'F' key (headlight function)

The Windsurfer can be picked up into inventory, if you sink it at any point, although be careful if you sink it too far away from the shores as it could be a long swim back. 
Zombies now can attack water vehicles in game.

No storage option vehicle.

The Windsurfer is a early game vehicle which is craftable in a standard workbench with no progression needed at the cost of gathering resources to build it,  it will help early game if you actually find a 'working workbench' and allow you to make the windsurfer a lot sooner.

Additional credits : ActiniumTiger for new texture for windsurfer . 

Happy Boating in A19

Ragsy!!

 

